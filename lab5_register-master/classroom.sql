-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2018 at 02:16 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `classroom`
--

-- --------------------------------------------------------

--
-- Table structure for table `cr_regist`
--

CREATE TABLE `cr_regist` (
  `r_id` int(11) NOT NULL,
  `r_s_id` int(2) NOT NULL,
  `r_sj_id` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cr_student`
--

CREATE TABLE `cr_student` (
  `s_id` int(2) NOT NULL,
  `s_first_name` varchar(50) NOT NULL,
  `s_last_name` varchar(50) NOT NULL,
  `s_user` varchar(50) NOT NULL,
  `s_pass` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cr_student`
--

INSERT INTO `cr_student` (`s_id`, `s_first_name`, `s_last_name`, `s_user`, `s_pass`) VALUES
(1, 'Peter', 'Parker', 'peter', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `cr_subject`
--

CREATE TABLE `cr_subject` (
  `sj_id` int(1) NOT NULL,
  `sj_name` varchar(50) NOT NULL,
  `sj_credit` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cr_subject`
--

INSERT INTO `cr_subject` (`sj_id`, `sj_name`, `sj_credit`) VALUES
(1, 'Web Programming', '3'),
(2, 'Artificial Intelligence', '3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cr_regist`
--
ALTER TABLE `cr_regist`
  ADD PRIMARY KEY (`r_id`,`r_s_id`,`r_sj_id`),
  ADD KEY `fk_student` (`r_s_id`),
  ADD KEY `fk_subject` (`r_sj_id`);

--
-- Indexes for table `cr_student`
--
ALTER TABLE `cr_student`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `cr_subject`
--
ALTER TABLE `cr_subject`
  ADD PRIMARY KEY (`sj_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cr_regist`
--
ALTER TABLE `cr_regist`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cr_student`
--
ALTER TABLE `cr_student`
  MODIFY `s_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cr_subject`
--
ALTER TABLE `cr_subject`
  MODIFY `sj_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cr_regist`
--
ALTER TABLE `cr_regist`
  ADD CONSTRAINT `fk_student` FOREIGN KEY (`r_s_id`) REFERENCES `cr_student` (`s_id`),
  ADD CONSTRAINT `fk_subject` FOREIGN KEY (`r_sj_id`) REFERENCES `cr_subject` (`sj_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
