<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->increments('ID_employee');
            $table->string("username" ,50) ;
            $table->string("password" , 50);
            $table->string("name" , 50);
            $table->string("phone" , 50);
            $table->string("e-mail" , 50);
            $table->string("address" , 50);
       
            $table->integer("ID_admin");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
