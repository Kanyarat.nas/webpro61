<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;

class BooksController extends Controller
{

    function  store(Request $request){
        if(Books::created($request->all())){
            return true ;
        }
    }
    function  update(Request $request , Books $book){
        if($book->fill($request->all())->save()){
            return true;
        }
    }
    function index (){
        $books = Books::all();
        return $books ;
    }
    function  show(Books $book){
        return $book;
    }

    function  destroy(Books $book){
        if($book->delete()){
            return true ;
        }
    }
    /*function  store(Request $request){
        $book = new Books();
        $book->setAttribute("name",$request->name);
        $book->setAttribute("category",$request->category);
        $book->setAttribute("description",$request->decription);
        if($book->save()){
            return true ;
        }
    }*/
}
